
public class Conta {
	private Cliente titular;
	private int numero;
	private static int cont;
	private double saldo, limite;
	
	Conta(){
		Conta.cont++;
		this.numero = Conta.cont;
	}
	
	public int getNumero(){
		return this.numero;
	}
	
	public boolean sacar(double valor){
		if(valor <= (this.saldo + this.limite)){
			this.saldo-=valor;
			return true;
		}
		return false;
	}
	
	public boolean depositar (double valor){
		if(valor>0){
			this.saldo += valor;
			return true;
		}
		return false;
	}
	
	public boolean transferir (Conta contaDestino, double valor){
		boolean sacou = this.sacar(valor);
		if(sacou){
			contaDestino.depositar(valor);
			return true;
		} else 
				return false;
	}
	
	public double getSaldo(){
		return this.saldo + this.limite;
	}
	
	public void setLimite(double lim){
		this.limite = lim;
	}
	
	public void setTitular(Cliente cl){
		this.titular = cl;
	}
	
	public Cliente getTitular(){
		return this.titular;
	}
	
	public void atualiza(double taxa){
		this.saldo -= taxa;
	}

}
