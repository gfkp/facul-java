
public class ContaCorrente extends Conta{
	public void atualiza(double taxa){
		super.atualiza(taxa * 2);
	}
	
	public boolean depositar(double valor){
		return super.depositar(valor - 0.1);
	}
}
