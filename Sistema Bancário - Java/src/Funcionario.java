
public class Funcionario{
	private String nome, endereco, cpf, departamento, cargo;
	private  int identificador;
	private static int cont;
	protected double salario;
	private EstadoFuncionario estado;
		
	Funcionario(String n){
		Funcionario.cont++;
		this.identificador = Funcionario.cont;
		this.nome=n;
		this.estado = EstadoFuncionario.EM_EXERCICIO;
	}
	
	public double getBonificacao(){
		return this.salario*0.10;
	}
	
	public static int getTotalFuncionario()
	{
		return cont;
	}
	
	public int getIdentificador(){
		return identificador;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public String getNome (){
		return this.nome;
	}
	
	public void setEndereco(String ende ){
		this.endereco = ende;
	}
	
	public String getEndereco(){
		return this.endereco;
	}

	public void setCpf(String cpf ){
		if(validaCpf(cpf))
			this.cpf = cpf;
	}
	
	public String getCpf(){
		return this.cpf;
	}
	
	public boolean validaCpf(String cpf){
		if(cpf.length()==11)
			return true;
		else
			System.out.println("Cpf inv�lido!");
		return false;
	}
	public void setDepartamento(String depart){
		this.departamento = depart;
	}
	
	public String getDepartamento(){
		return this.departamento;
	}
	
	public void setCargo(String cargo){
		this.cargo = cargo;
	}
	
	public String getCargo(){
		return this.cargo;
	}
	
	public void setSalario(double sal){
		this.salario = sal;
	}
	
	public double getSalario(){
		return this.salario;
	}
	
	public double getSaldo(){
		return this.salario;
	}
	
	public void aumentarSalario(double percent){
		salario += salario*(percent/100.0);
	}
	
	public void caracteristicasFuncionario (){
		System.out.println("Nome: " + this.nome + "\nEndereco: " + this.endereco + "\nCpf: " + this.cpf + "\nDepartamento: "
							+ this.departamento + "\nCargo: " + this.cargo + "\nSalario: " + this.salario + "\nEstado: " + this.estado);
	}
	
	public void setEstado(EstadoFuncionario estado){
		this.estado = estado;
	}
	
	public EstadoFuncionario getEstado(){
		return this.estado;
	}
}
