
public class TestaContas {
	public static void main(String[] args) {
		Conta c = new Conta();
		ContaCorrente cc = new ContaCorrente();
		ContaPoupanca cp = new ContaPoupanca();
		
		c.depositar(100);
		cc.depositar(100);
		cp.depositar(100);
		
		System.out.println("Saldos: ");
		System.out.println("c : "+ c.getSaldo());
		System.out.println("cc : "+ cc.getSaldo());
		System.out.println("cp : "+ cp.getSaldo());
		
		c.atualiza(10);
		cc.atualiza(10);
		cp.atualiza(10);
		
		System.out.println("\nSaldos p�s atualiza��o: ");
		System.out.println("c : "+ c.getSaldo());
		System.out.println("cc : "+ cc.getSaldo());
		System.out.println("cp : "+ cp.getSaldo());
	}
}
