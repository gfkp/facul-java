
public class ControleBanco {

	public static void main(String[] args) {
		Banco meuBanco = new Banco("Banco do Matheus","Rua 0","11111111110");
		
		meuBanco.contratarFuncionario("Jo�o", "Rua 1", "11111111111", "TI", "Programador", 1000);
		meuBanco.contratarFuncionario("Maria", "Rua 2", "11111111112", "TI", "Analista", 2000);
		meuBanco.contratarFuncionario("Jos�", "Rua 3", "11111111113", "TI", "Gerente", 4000);
		
		meuBanco.criarConta("Carol", "da Silva", "11111111114", "Rua 4", 1000);
		meuBanco.criarConta("Pedro", "de Oliveira", "11111111115", "Rua 5", 2000);
	
		meuBanco.mostrarSaldoTotal();
		meuBanco.mostrarTotalGastos();
		
		meuBanco.demitirFuncionario("11111111111");
		meuBanco.excluirConta("11111111115");
		
		meuBanco.mostrarSaldoTotal();
		meuBanco.mostrarTotalGastos();
		
		meuBanco.imprimirListaDeFuncionarios();
		
		/*Funcionario f = new Funcionario("Joao"); 
		Gerente g1 = new Gerente("Pedro");
		Gerente g2 = new Gerente("Maria");
		
		System.out.println("Nome: "+f.getNome());
		System.out.println("Id:"+f.getIdentificador());
		
		System.out.println("Nome: "+g1.getNome());
		System.out.println("Id: "+g1.getIdentificador());
		
		System.out.println("Nome: "+g2.getNome());
		System.out.println("Id: "+g2.getIdentificador());
		
		System.out.println("Total de gerentes: "+Gerente.getTotalGerentes());
		System.out.println("Total de funcionarios: "+Funcionario.getTotalFuncionario());*/
	}
}
