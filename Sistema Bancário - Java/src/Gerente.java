
public class Gerente extends Funcionario{
	private int senha;
	private static int totalGerente;
	
	Gerente(String n){
		super(n);
		Gerente.totalGerente++;
	}
	
	public boolean autentica(int senha)
	{
		if(this.senha==senha){
			System.out.println("Acesso Permitido!");
			return true;
	}
		else return false;
	}
	
	public void setSenha(int senha)
	{
		this.senha = senha;
	}
	
	public static int getTotalGerentes()
	{
		return totalGerente;
	}
	
	public double getBonificacao(){
		return super.getBonificacao() * 2;
	}
}
