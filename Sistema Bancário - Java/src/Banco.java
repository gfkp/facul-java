
public class Banco {
	private String nome;
	private String endereco;
	private String CNPJ;
	private Conta [] contas = new Conta[10];
	private Funcionario [] funcionarios = new Funcionario[10];	
	
	Banco (String nomeB,String enderecoB, String cnpj){
		setNome(nomeB);
		setEndereco(enderecoB);
		setCNPJ(cnpj);
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public String getNome(){
		return this.nome;
	}
	
	public void setEndereco(String ende){
		this.endereco = ende;
	}
	
	public String getEndereco(){
		return this.endereco;
	}
	
	public void setCNPJ(String cnpj){
		this.CNPJ = cnpj;
	}
	
	public String getCNPJ(){
		return this.CNPJ;
	}
	
	public boolean criarConta(String nomeCliente, String sobrenomeCliente, String cpfCliente, String enderecoCliente, double deposito){
		for(int i=0; i<this.contas.length; i++){
			if(this.contas[i]==null){
				Conta novaConta = new Conta();
				Cliente novoCliente = new Cliente();
				novaConta.setTitular(novoCliente);
				novaConta.getTitular().setNome(nomeCliente);
				novaConta.getTitular().setSobreome(sobrenomeCliente);
				novaConta.getTitular().setCpf(cpfCliente);
				novaConta.getTitular().setEndereco(enderecoCliente);
				novaConta.depositar(deposito);
				this.contas[i] = novaConta;
				return true;
			}
		}
		return false;
	}
	
	public boolean excluirConta(String cpfConta){
		for(int i=0; i<this.contas.length; i++){
			if(this.contas[i]!=null){
				if(this.contas[i].getTitular().getCpf() == cpfConta){
					this.contas[i] = null;
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean contratarFuncionario(String nomeFuncionario, String enderecoFuncionario, String cpfFuncionario, String departamento, String cargo, double salario){
		for(int i=0; i<this.funcionarios.length; i++){
			if(this.funcionarios[i]==null){
				Funcionario novoFunc = new Funcionario(nomeFuncionario);
				novoFunc.setNome(nomeFuncionario);
				novoFunc.setEndereco(enderecoFuncionario);
				novoFunc.setCpf(cpfFuncionario);
				novoFunc.setDepartamento(departamento);
				novoFunc.setCargo(cargo);
				novoFunc.setSalario(salario);
				this.funcionarios[i] = novoFunc;
				return true;
			}
		}
		return false;
	}
	
	public boolean demitirFuncionario(String cpf){
		for(int i=0; i<this.funcionarios.length; i++){
			if(this.funcionarios[i]!=null){
				if(this.funcionarios[i].getCpf() == cpf){
					this.funcionarios[i] = null;
					return true;
				}
			}
		}
		return false;
	}
	
	public void mostrarSaldoTotal(){
		double totalsaldo = 0;
		for(int i=0; i<this.contas.length; i++)
			if(this.contas[i]!=null)
				totalsaldo += this.contas[i].getSaldo();
		System.out.println("Saldo Total: "+totalsaldo);
	}
	
	public void mostrarTotalGastos(){
		double totalgastos = 0;
		for(int i=0; i<this.funcionarios.length; i++)
			if(this.funcionarios[i] != null && this.funcionarios[i].getEstado() == EstadoFuncionario.EM_EXERCICIO)
				totalgastos += this.funcionarios[i].getSalario();
		System.out.println("Total de Gastos: "+totalgastos+"\n");
	}
	
	public void imprimirListaDeFuncionarios(){
		for(int i = 0; i < this.funcionarios.length; i++){
			if(this.funcionarios[i] instanceof Funcionario){
				System.out.println("Funcionario "+i);
				System.out.println("Nome : "+this.funcionarios[i].getNome());
				System.out.println("Endere�o : "+this.funcionarios[i].getEndereco());
				System.out.println("CPF : "+this.funcionarios[i].getCpf());
				System.out.println("Departamento : "+this.funcionarios[i].getDepartamento());
				System.out.println("Identificador : "+this.funcionarios[i].getIdentificador());
				System.out.println("\n");
			}
		}
	}
}