
public class Cliente {
	private String nome;
	private String sobrenome;
	private String endereco;
	private String cpf;
	
	public void setNome(String n){
		this.nome = n;
	}
	
	public String getNome(){
		return this.nome;
	}
	
	public void setSobreome(String sn){
		this.sobrenome = sn;
	}
	
	public String getSobrenome(){
		return this.sobrenome;
	}
	
	public void setEndereco(String ende){
		this.endereco = ende;
	}
	
	public String getEndereco(){
		return this.endereco;
	}
	
	public void setCpf(String cpf){
		if(validaCpf(cpf))
			this.cpf = cpf;
	}
	
	public String getCpf(){
		return this.cpf;
	}
	
	public boolean validaCpf(String cpf){
		if(cpf.length()==11)
			return true;
		else
			System.out.println("Cpf inv�lido!");
		return false;
	}
}
