
public enum EstadoFuncionario {
	EM_EXERCICIO("Em exerc�cio"),
	EM_FERIAS("Em f�rias"),
	AFASTADO_POR_DOENCA("Afastado por doen�a"),
	EXONERADO("Exonerado"),
	APOSENTADO("Aposentado");
	
	private String estado;
	
	EstadoFuncionario(String estado){
		this.estado = estado;
	}
	
	public String toString(){
		return this.estado;
	}
}
